#-*-coding:utf-8-*-
# !/usr/bin/env python

import pymongo
import datetime
import envoy
import os
import sys

from pymongo import MongoClient

# Making a Connection with MongoClient
client = MongoClient('localhost', 27017)

# Getting a Database
db = client['movielens']
db.connection.drop_database('movielens')
