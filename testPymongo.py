#-*-coding:utf-8-*-
# !/usr/bin/env python

import pymongo
import datetime
import numpy as np
import envoy

from pymongo import MongoClient

# Making a Connection with MongoClient
client = MongoClient('localhost', 27017)

# Getting a Database
#db = client.test_database
db = client['testdatabase']

#r = client['mongoimport --db testdatabase --file u.data.json']


## Getting a Collection
Collection = db['test-collection']
#
## Documents
#post = {"author": "Mike", "text": "My first blog post!", "tags": ["mongodb", "python", "pymongo"], "date": datetime.datetime.utcnow()}
#
## Inserting a Document
#posts = db.posts
#post_id = posts.insert(post)
#
#print post_id
#
#print db.collection_names()
#
#print posts.find_one()
#print posts.find_one({"author": "Mike"})
#print posts.find_one({"author": "Eliot"})
#print posts.find_one({"_id": post_id})
#
#new_posts = [{"author": "Mike", "text": "Another post!", "tags": ["bulk", "insert"], "date": datetime.datetime(2009, 11, 12, 11, 14)},
#             {"author": "Eliot", "title": "MongoDB is fun", "text": "and pretty easy too!", "date": datetime.datetime(2009, 11, 10, 10, 45)}]
#
#posts.insert(new_posts)
#
## Querying for More Than One Document
#for post in posts.find():
#    print post
#
#for post in posts.find({"author": "Mike"}):
#    print post
#
#print posts.count()
#db.connection.drop_database('test-database')



