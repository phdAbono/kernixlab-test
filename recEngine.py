#-*-coding:utf-8-*-
#!/usr/bin/env python

import envoy
import pymongo
import json
from bson import json_util
from pymongo import MongoClient
from datetime import datetime as dt
import numpy as np
import pylab as pl
import sys
import time

############################
# Functions
############################

def mongo(db, cmd):
    """
    Simulates a mongo shell using envoy to execute commands
    Input:
    - db: database
    - cmd: cmd to be executed
    """
    r = envoy.run("mongo %s --eval 'printjson(%s)'" % (db, cmd,))
    print r.std_out
    if r.std_err: print r.std_err

############################

def pp(o, indent=1):
    """
    Creates a convenience function to make pretty-printing JSON a little
    less cumbersome
    """
    print json.dumps(o, indent=indent, default=json_util.default)

############################

def computeIndexAge(tStartBSON, tEndBSON, tBSON):
    """
    Computes Item Age Index
    Input:
    Output:
    """
    indexAge = float(tBSON - tStartBSON)/float(tEndBSON - tStartBSON)
    return indexAge

############################

def convert2BSON(datetimeDat):
    """
    """
    tBSON = int( 100*(time.mktime(datetimeDat.timetuple()))+3600)
    return tBSON

############################

def buildFeaturesVecItem(itemid, averageRating, items, ageIndexVec, nbFeatures, scalingFacRat=1, scalingFacAge=1):
    """
    Buils Features Vector for representing Item Profiles
    Input:
    - itemid: item id
    - averageRating: average item Rating Vector
    - scalingFac: scaling Factor
    Output:
    - itemVec: Features Vector for item
     |averageRating| Age Index |  Action | Adventure | Animation |
     Children's | Comedy | Crime | Documentary | Drama | Fantasy |
     Film-Noir | Horror | Musical | Mystery | Romance | Sci-Fi |
    Thriller | War | Western |
    """
#    print "itemid: %d " %itemid

    itemVec = np.zeros(nbFeatures)
    item = [it for it in  items.find({"movie id": str(itemid)})]

    # Average Rating
    itemVec[0] = (averageRating[itemid-1]-2.5) * scalingFacRat
    # Age Index
    itemVec[1] = ageIndexVec[itemid-1] * scalingFacAge
    # Movie Categories
    itemVec[2] = item[0]['Action']
    itemVec[3] = item[0]['Adventure']
    itemVec[4] = item[0]["Animation"]
    itemVec[5] = item[0]["Children's"]
    itemVec[6] = item[0]['Comedy']
    itemVec[7] = item[0]['Crime']
    itemVec[8] = item[0]['Documentary']
    itemVec[9] = item[0]['Drama']
    itemVec[10] = item[0]['Fantasy']
    itemVec[11] = item[0]['Film-Noir']
    itemVec[12] = item[0]['Horror']
    itemVec[13] = item[0]['Musical']
    itemVec[14] = item[0]['Mystery']
    itemVec[15] = item[0]['Romance']
    itemVec[16] = item[0]['Sci-Fi']
    itemVec[17] = item[0]['Thriller']
    itemVec[18] = item[0]['War']
    itemVec[19] = item[0]['Western']

    return itemVec

def buildFeaturesVecUser(userId, averageRatingUser, weightedUMat, nbFeatures, ageIndexVec, itemMat, alpha1, alpha2):
    """
    Buils Features Vector for representing User Profiles.
    Input:
    - userId:
    - averageRatingUser:
    - users:
    - items:
    - nbFeatures
    Output;
    - userVec: Features Vector for user
    |averageRatingUser| Average Age Index | Average Action | Average Adventure | Average Animation |
    Average Children's | Average Comedy | Average Crime | Average Documentary | Average Drama | Average Fantasy |
    Average Film-Noir | Average Horror | Average Musical | Average Mystery | Average Romance | Average Sci-Fi |
    Average Thriller | Average War | Average Western |
    """
    userVec = np.zeros(nbFeatures)

    # Average Rating per User
    
    userVec[0] = alpha1 * (averageRatingUser[userId-1]-5)
    
    ind = np.where(weightedUMat[userId-1,:]!=0)
    nRatings = ind[0].shape[0]
    totAge = np.sum(ageIndexVec[ind])/nRatings # Average Age 
    userVec[1] = alpha2 *totAge
    
    #  Movie Categories

    indAct = np.where(itemMat[:,2]!=0)
    indUserAct = np.intersect1d(ind[0], indAct[0])
    nRatAct = float(indUserAct.shape[0])
    if(nRatAct!=0):
        userVec[2] = np.sum(weightedUMat[userId-1, indUserAct])/nRatAct
    else:
        userVec[2] = 0

    indAdv = np.where(itemMat[:,3]!=0)
    indUserAdv = np.intersect1d(ind[0], indAdv[0])
    nRatAdv = float(indUserAdv.shape[0])
    if(nRatAdv !=0):
        userVec[3] = np.sum(weightedUMat[userId-1, indUserAdv])/nRatAdv
    else:
        userVec[3] = 0

    indAni = np.where(itemMat[:,4]!=0)
    indUserAni = np.intersect1d(ind[0], indAni[0])
    nRatAni = float(indUserAni.shape[0])

    if(nRatAni!=0):
        userVec[4] = np.sum(weightedUMat[userId-1, indUserAni])/nRatAni
    else:
        userVec[4] = 0

    indChi = np.where(itemMat[:,5]!=0)
    indUserChi = np.intersect1d(ind[0], indChi[0])
    nRatChi = float(indUserChi.shape[0])
    if(nRatChi !=0):
        userVec[5] = np.sum(weightedUMat[userId-1, indUserChi])/nRatChi
    else:
        userVec[5] = 0

    indCom = np.where(itemMat[:,6]!=0)
    indUserCom = np.intersect1d(ind[0], indCom[0])
    nRatCom = float(indUserCom.shape[0])
    if(nRatCom !=0):
        userVec[6] = np.sum(weightedUMat[userId-1, indUserCom])/nRatCom
    else:
        userVec[6] = 0

    indCri = np.where(itemMat[:,7]!=0)
    indUserCri = np.intersect1d(ind[0], indCri[0])
    nRatCri = float(indUserCri.shape[0])
    if(nRatCri !=0):
        userVec[7] = np.sum(weightedUMat[userId-1, indUserCri])/nRatCri
    else:
        userVec[7] = 0

    indDoc = np.where(itemMat[:,8]!=0)
    indUserDoc = np.intersect1d(ind[0], indDoc[0])
    nRatDoc = float(indUserDoc.shape[0])
    if(nRatDoc!=0):
        userVec[8] = np.sum(weightedUMat[userId-1, indUserDoc])/nRatDoc
    else:
        userVec[8] = 0

    indDra = np.where(itemMat[:,9]!=0)
    indUserDra = np.intersect1d(ind[0], indDra[0])
    nRatDra = float(indUserDra.shape[0])
    if(nRatDra!=0):
        userVec[9] = np.sum(weightedUMat[userId-1, indUserDra])/nRatDra
    else:
        userVec[9] = 0

    indFan = np.where(itemMat[:,10]!=0)
    indUserFan = np.intersect1d(ind[0], indFan[0])
    nRatFan = float(indUserFan.shape[0])
    if(nRatFan!=0):
        userVec[10] = np.sum(weightedUMat[userId-1, indUserFan])/nRatFan
    else:
        userVec[10] = 0

    indFil = np.where(itemMat[:,11]!=0)
    indUserFil = np.intersect1d(ind[0], indFil[0])
    nRatFil = float(indUserFil.shape[0])
    if(nRatFil!=0):
        userVec[11] = np.sum(weightedUMat[userId-1, indUserFil])/nRatFil
    else:
        userVec[11] = 0

    indHor = np.where(itemMat[:,12]==1)
    indUserHor = np.intersect1d(ind[0], indHor[0])
    nRatHor = float(indUserHor.shape[0])
    if(nRatHor!=0):
        userVec[12] = np.sum(weightedUMat[userId-1, indUserHor])/nRatHor
    else:
        userVec[12] = 0

    indMus = np.where(itemMat[:,13]==1)
    indUserMus = np.intersect1d(ind[0], indMus[0])
    nRatMus = float(indUserMus.shape[0])
    if(nRatMus!=0):
        userVec[13] = np.sum(weightedUMat[userId-1, indUserMus])/nRatMus
    else:
        userVec[13] = 0

    indMys = np.where(itemMat[:,14]==1)
    indUserMys = np.intersect1d(ind[0], indMys[0])
    nRatMys = float(indUserMys.shape[0])
    if(nRatMys!=0):
        userVec[14] = np.sum(weightedUMat[userId-1, indUserMys])/nRatMys
    else:
        userVec[14] = 0

    indRom = np.where(itemMat[:,15]==1)
    indUserRom = np.intersect1d(ind[0], indRom[0])
    nRatRom = float(indUserRom.shape[0])
    if(nRatRom!=0):
        userVec[15] = np.sum(weightedUMat[userId-1, indUserRom])/nRatRom
    else:
        userVec[15] = 0 

    indSci = np.where(itemMat[:,16]==1)
    indUserSci = np.intersect1d(ind[0], indSci[0])
    nRatSci = float(indUserSci.shape[0])
    if(nRatSci !=0):
        userVec[16] = np.sum(weightedUMat[userId-1, indUserSci])/nRatSci
    else:
        userVec[16] = 0

    indThr = np.where(itemMat[:,17]==1)
    indUserThr = np.intersect1d(ind[0], indThr[0])
    nRatThr = float(indUserThr.shape[0])
    if(nRatThr!=0):
        userVec[17] = np.sum(weightedUMat[userId-1, indUserThr])/nRatThr
    else:
        userVec[17] = 0

    indWar = np.where(itemMat[:,18]==1)
    indUserWar = np.intersect1d(ind[0], indWar[0])
    nRatWar = float(indUserWar.shape[0])
    if(nRatWar!=0):
        userVec[18] = np.sum(weightedUMat[userId-1, indUserWar])/nRatWar
    else:
        userVec[18] = 0

    indWes = np.where(itemMat[:,19]==1)
    indUserWes = np.intersect1d(ind[0], indWes[0])
    nRatWes = float(indUserWes.shape[0])
    if(nRatWes!=0):
        userVec[19] = np.sum(weightedUMat[userId-1, indUserWes])/nRatWes
    else:
        userVec[19] = 0

    return userVec

############################ 

def cosineDistance(vec1, vec2):
    """
    Computes the cosine distance between two vectors
    Input:
    - vec1:
    - vec2:
    Output:
    - cd: cosine distance between vec1 and vec2
    """
    cd = np.dot(vec1, vec2)
    cd = cd/(np.linalg.norm(vec1,2) * np.linalg.norm(vec2,2))
    return cd

############################ 

def euclideanDistance(vec1,vec2):
    """
    Computes the euclidean distance between two vectors
    """
    ed = np.sum(np.power(vec1 - vec2, 2))
    return ed

############################
# Main
############################

print "============================"    
mongo('movielens', 'db.ratings.stats()')
mongo('movielens', 'db.items.stats()')
mongo('movielens', 'db.users.stats()')
print "============================"

# Connects to Mongo server
client = MongoClient('localhost', 27017)

db = client.movielens
ratings = db.ratings
items = db.items
users = db.users

print "Number of ratings in ratings: %d" %ratings.count()
print "Number of items in ratings: %d" %items.count()
print "Number of users in ratings: %d" %users.count()
print "============================"

nbRatings = ratings.count()
nbItems = items.count()
nbUsers = users.count()

# Pick one rating, one item, one user

rating = ratings.find_one()
item = items.find_one()
user = users.find_one()

print "A rating: "
print json.dumps(rating, indent=1, default=json_util.default)
print "============================"
print "An item: "
print json.dumps(item, indent=1, default=json_util.default)
print "============================"
print "A user: "
print json.dumps(user, indent=1, default=json_util.default)
print "============================"

# Querying MongoDB  by date

start_date = dt(1990, 1, 1)# Year, Month, Day
end_date = dt(1995, 11, 1)# Year, Month, Day
#print start_date

#ratingsDate = [ rating 
#              for rating in ratings.find({"timestamp" : 
#                                  {
#                                   "$lt" : end_date, 
#                                   "$gt" : start_date
#                                  }
#                              }).sort("date")]
#
#moviesDate = [ movie 
#              for movie in items.find({"timestamp" : 
#                                  {
#                                   "$lt" : end_date, 
#                                   "$gt" : start_date
#                                  }
#                              }).sort("date")]
#
#print "Ratings from a date range"
#pp(ratingsDate)
#
#print "Movie from a date range"
#pp(moviesDate)


print "===================="
print "Computing Age Index"

start_date = dt(1900, 1, 1) #Year, Month, Day
end_date = dt(2014, 11, 1)  #Year, Month, Day

moviesDate = [ movie 
              for movie in items.find({"timestamp" : 
                                  {
                                   "$lt" : end_date, 
                                   "$gt" : start_date
                                  }
                              }).sort("timestamp")]

tStart = moviesDate[0]["timestamp"]
tEnd = moviesDate[-1]["timestamp"]
tEndBSON = convert2BSON(tEnd)
tStartBSON = convert2BSON(tStart)

ageIndexVec = np.zeros(nbItems)
numWeird = 267
for i in range(1,nbItems+1):
#    print "Age Item: %d/%d" %(i, nbItems)

    movie = [ movie 
              for movie in items.find({"movie id" : str(i)})]
    if(i==numWeird):
        print "unknown type"

    else:
        ti = movie[0]["timestamp"]
        tiBSON = convert2BSON(ti)
        ageIndexVec[i-1] = computeIndexAge(tStartBSON, tEndBSON, tiBSON)

filename = "npy/ageIndexVec.npy"
np.save(filename, ageIndexVec)

#### Utility Matrix

print "========================="
print "Computing the Utility Matrix"

utilityMat = np.zeros((nbUsers, nbItems))
for i in range(1,nbUsers+1):
#    print "User: %d/%d" %(i, nbUsers)
    itemsi = [it for it in ratings.find({"user id": str(i)})]
    nbItemsi = len(itemsi)

    for j in range(nbItemsi):
        indexItem = itemsi[j]["item id"]
        utilityMat[i-1, int(indexItem)-1] = itemsi[j]["rating"]

filename = 'npy/utilityMat.npy'
np.save(filename, utilityMat)        

# Computing average rating per Movie
print "======================="
print "Computing Average Rating per Movie"
averageRating = np.zeros(nbItems)

for j in range(1, nbItems+1):
#    print "Movie: %d/%d" %(j, nbItems)
    addition = np.sum(utilityMat[:,j-1])
    rtgMovie = [rtg for rtg in ratings.find({"item id": str(j)})]
    njratings = len(rtgMovie)
    if (njratings == 0):
        averageRating[j-1] = 0
    else:
        njratings = float(njratings)
        averageRating[j-1] = addition/njratings

filename = 'npy/averageRating.npy'
np.save(filename, averageRating)

print "============================"
print "Computing Features Vector per Item"

filename = 'npy/ageIndexVec.npy'
ageIndexVec = np.load(filename)

filename = 'npy/utilityMat.npy'
utilityMat = np.load(filename)

filename = 'npy/averageRating.npy'
averageRating = np.load(filename)

nbFeatures = 20

itemFeaturesMat = np.zeros((nbItems, nbFeatures))

alpha1 = 1
alpha2 = 0.5
for i in range(1, nbItems+1):
    itemFeaturesMat[i-1,:] = buildFeaturesVecItem(i, averageRating, items, ageIndexVec, nbFeatures, alpha1, alpha2)

filename = 'npy/itemFeaturesMat.npy'
np.save(filename, itemFeaturesMat)

print "========================="
print "Loading Useful Data"

filename = 'npy/ageIndexVec.npy'
ageIndexVec = np.load(filename)

filename = 'npy/utilityMat.npy'
utilityMat = np.load(filename)

filename = 'npy/averageRating.npy'
averageRating = np.load(filename)

#filename = 'npy/itemFeaturesMat.npy'
#itemFeaturesMat = np.load(filename)

print "========================="
print "Computing User Profiles"

# To describe the user's preferences
# Weight by substracting the average rating per item
# Average Rating per User

weightedUMat = np.zeros((nbUsers, nbItems))
averageRatingUser = np.zeros((nbUsers))

for i in range(1,nbUsers+1):
#    print "User: %d/%d" %(i, nbUsers)
    ind = np.where(utilityMat[i-1,:]>0) #index of the rated items
    meanRating = np.mean(utilityMat[i-1, ind[0]])
#    print "Average Rating for User %d: %.2f" %(i,meanRating)
    averageRatingUser[i-1] = meanRating
    weightedUMat[i-1, ind[0]] = utilityMat[i-1, ind[0]] - meanRating

filename = 'npy/averageRatingUser.npy'
np.save(filename, averageRatingUser)
#averageRatingUser = np.load(filename)

filename = 'npy/weightedUMat.npy'
np.save(filename, weightedUMat)
#weightedUMat = np.load(filename)



userMat = np.zeros((nbUsers, nbFeatures))
for i in range(1, nbUsers+1):
    userMat[i-1,:] = buildFeaturesVecUser(i, averageRatingUser, weightedUMat, nbFeatures, ageIndexVec, itemFeaturesMat, alpha1, alpha2)

# Saving/ Loading
filename = 'npy/userMat.npy'
#np.save(filename, userMat)
#userMat = np.load(filename)

print "========================="
print "Computing all the Cosine Distances"

cdMat = np.zeros((nbUsers, nbItems))

for i in range(nbUsers):
#    print "Cd: %d/%d" %(i+1, nbUsers)
    alreadySeen = np.where(weightedUMat[i,:]!=0)
    indSeen =  alreadySeen[0]

    for j in range(nbItems):
        if(j not in indSeen):
            cdMat[i,j] = cosineDistance(userMat[i,:], itemFeaturesMat[j,:])
        else:
            cdMat[i,j] = 1

## We do not want to recommend the unknown movie
#unknownId = 266
cdMat[:,numWeird-1] = 1

# Saving Loading
#filename = 'npy/cdMat.npy'
#np.save(filename, cdMat)
#cdMat = np.load(filename)

# nb of Movie to recommend
nbMovies = 3

print "=============================="
print "Recommending: "
for i in range(1, nbUsers+1):
    print "===="
    print "for User %d" %i
    print "===="
    indRec0 =  np.argsort(np.abs(cdMat[i-1,:]))[:nbMovies]
    indRec0 = indRec0+1
    nbMoviesFin = indRec0.shape[0]
    for k in range(nbMoviesFin):
        movie = [mv for mv in items.find({"movie id": str(indRec0[k])})]
        print "%s st Rec: %s avRat: %.2f" %(k+1, movie[0]["movie title"], averageRating[indRec0[k]-1])
    
print "========================="
print "Display"

filenamefig = "png/utilityMat.png"
pl.figure()
pl.imshow(utilityMat, aspect='auto')
pl.title('Utility Matrix')
pl.ylabel('User')
pl.xlabel('Movie')
pl.colorbar()
pl.savefig(filenamefig)

filenamefig = 'png/averageRating.png'
pl.figure()
pl.plot(averageRating, 'o')
pl.title("Average Rating per movie")
pl.xlabel("Movie")
pl.ylabel("Rating")
pl.savefig(filenamefig)

filenamefig = 'png/ageIndex.png'
pl.figure()
pl.plot(ageIndexVec, 'o')
pl.title("Age Index per movie")
pl.xlabel("Movie")
pl.ylabel("Age")
pl.savefig(filenamefig)

filenamefig = 'png/itemFeaturesMat.png'
pl.figure()
pl.imshow(itemFeaturesMat,aspect='auto' )
pl.title("Item Features")
pl.xlabel("Features")
pl.ylabel("Movies")
pl.savefig(filenamefig)
pl.colorbar()

filenamefig = 'png/averageRatingUser.png'
pl.figure()
pl.plot(averageRatingUser, 'o')
pl.title("Average Rating per User")
pl.xlabel("User")
pl.ylabel("Average Rating")
pl.savefig(filenamefig)

filenamefig = 'png/weightedUMat.png'
pl.figure()
pl.imshow(weightedUMat, aspect='auto')
pl.title('Weighted Utility Matrix')
pl.ylabel('User')
pl.xlabel('Movie')
pl.colorbar()
pl.savefig(filenamefig)

filenamefig = 'png/userMat.png'
pl.figure()
pl.imshow(userMat, aspect='auto')
pl.title('User Features Mat')
pl.ylabel('User')
pl.xlabel('Features')
pl.colorbar()
pl.savefig(filenamefig)

filenamefig = 'png/cdMat.png'
pl.figure()
pl.imshow(cdMat, aspect='auto')
pl.title('Cd  Mat')
pl.ylabel('User')
pl.xlabel('Item')
pl.colorbar()
pl.savefig(filenamefig)

filename = 'png/cdMat0.png'
pl.figure()
pl.plot(np.abs(cdMat[0,:]),'o')
pl.xlabel('Item')
pl.title('Preference for user 0')
pl.savefig(filename)

#pl.show()

