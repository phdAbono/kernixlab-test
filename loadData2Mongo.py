#-*-coding:utf-8-*-
# !/usr/bin/env python

import pymongo
import datetime
import envoy
import os
import sys

from pymongo import MongoClient

# Making a Connection with MongoClient
client = MongoClient('localhost', 27017)

# Getting a Database
db = client['movielens']
#db.connection.drop_database('movielens')
## Getting collections
ratings = db['ratings']
items = db['items']
users = db['users']

# Getting the options for the mongoimport command
#r = envoy.run('mongoimport')
#print r.std_out
#print r.std_err
#
# Using mongoimport to load data into MongoDB
data_file = os.path.join(os.getcwd(), 'json/u.data.json')
## Run a command just as you would in a terminal on the virtual machine to 
## import the data file into MongoDB.
r = envoy.run('mongoimport --db movielens --collection ratings ' + \
              '--file %s' % data_file)

print r.std_out
print r.std_err

print "Data Added"
print "================="
users_file = os.path.join(os.getcwd(), 'json/u.user.json')

## Run a command just as you would in a terminal on the virtual machine to 
## import the data file into MongoDB.
r = envoy.run('mongoimport --db movielens --collection users ' + \
              '--file %s' % users_file)

print r.std_out
print r.std_err
print "Users Added"
print "================="
items_file = os.path.join(os.getcwd(), 'json/u.item.json')
## Run a command just as you would in a terminal on the virtual machine to 
## import the data file into MongoDB.
r = envoy.run('mongoimport --db movielens --collection items ' + \
              '--file %s' % items_file)
#
#
print r.std_out
print r.std_err
print "Items Added"
print "================="

#db.connection.drop_database('movielens')
