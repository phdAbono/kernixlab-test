#-*-coding:utf-8-*-
#!/usr/bin/env python

import json
import time
import struct
import datetime
import sys

class Encoder(json.JSONEncoder):
    def default(self, o): return  list(o)

def convertDate2UnixTS(date):
    """
    Converts a Date in a Unix timestamp in ms (mongodB format)
    Input:
      - date: data format (day-Month-year)
    Output:
      - unixTS: Unix timestamps (ms)
    """
    print "date: %s"  %date
    unixTS = time.mktime(datetime.datetime.strptime(date, "%d-%b-%Y").timetuple())
    unixTS = unixTS*1000
    unixTS = int(unixTS)
    return unixTS

def convertDateTime(date):
    """
    Converts a Date in a Specific format
    """
    u = datetime.datetime.strptime(date, "%d-%b-%Y")    
    return u

def bson_datetime(adatetime):
    """
    Converts a date in a BSON Format
    """
    try:
        ret = int(1000*(time.mktime(adatetime.timetuple()) + 3600))
        if ret < 0:
            ret = struct.unpack('Q', struct.pack('q', ret))[0]
        return {'$date': ret}
    except ValueError:
        return None

def finalConvert(date):
    """
    Converts Date from data format to BSON Format
    """
    u = datetime.datetime.strptime(date, "%d-%b-%Y")    
    return bson_datetime(u)


repos = 'ml-100k/'
filename = 'u.data'
outputFileName = 'json/' + filename + '.json'
absFilename = repos + filename

# Ratings

inputFile = open(absFilename, 'r')    
outputFile = open(outputFileName,'w')

for line in inputFile:
    
    if line != None:
        num = line.split()
        tmp = num[3]
        tmp = tmp + str(0) + str(0) + str(0)
        tmp = int(tmp)
        jsonLine = json.dumps({"user id": num[0], "item id": num[1], "rating": num[2], "timestamp": {'$date' : tmp}}, cls=Encoder)

        outputFile.write(jsonLine + '\n')                                                                        
outputFile.close()
inputFile.close()

# Items

filename ='u.item'
outputFileName = 'json/'+filename + '.json'
absFilename = repos + filename

inputFile = open(absFilename, 'r')

outputFile = open(outputFileName,'w')

for line in inputFile:
    if line != None:

        num = line.split('|')
        if(num[1]=="unknown"):
            print "Unknown movie"
            jsonLine = json.dumps({"movie id": num[0], "movie title": num[1], "timestamp": '', "IMDb URL": '', "unknown": num[5], "Action": num[6],
                               "Adventure": num[7], "Animation": num[8], "Children's": num[9], "Comedy": num[10], "Crime": num[11], "Documentary": num[12], "Drama": num[13],
                               "Fantasy": num[14], "Film-Noir": num[15], "Horror": num[16], "Musical": num[17], "Mystery": num[18], "Romance": num[19], "Sci-Fi": num[20], "Thriller": num[21],
                               "War": num[22], "Western": num[23][:-1]},cls=Encoder)
            outputFile.write(jsonLine + '\n')
        else:
            jsonLine = json.dumps({"movie id": num[0], "movie title": num[1], "timestamp": finalConvert(num[2]), "IMDb URL": num[4], "unknown": num[5], "Action": num[6],
                               "Adventure": num[7], "Animation": num[8], "Children's": num[9], "Comedy": num[10], "Crime": num[11], "Documentary": num[12], "Drama": num[13],
                               "Fantasy": num[14], "Film-Noir": num[15], "Horror": num[16], "Musical": num[17], "Mystery": num[18], "Romance": num[19], "Sci-Fi": num[20], "Thriller": num[21],
                               "War": num[22], "Western": num[23][:-1]},cls=Encoder)
            outputFile.write(jsonLine + '\n')

outputFile.close()
inputFile.close()

# Users

filename ='u.user'
outputFileName = 'json/' +filename + '.json'

absFilename = repos + filename
print absFilename
inputFile = open(absFilename, 'r')    
outputFile = open(outputFileName,'w')

for line in inputFile:
    if line != None:
        num = line.split('|')
        jsonLine = json.dumps({"user id": num[0], "age": num[1], "gender": num[2], "occupation": num[3], "zipcode": num[4][:-1]}, cls=Encoder)
        outputFile.write(jsonLine + '\n')

outputFile.close()
inputFile.close()
    
